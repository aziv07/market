import { Component, OnInit } from '@angular/core';

import {ShoppingCartService} from '../services/shopping-cart.service';
import {ShoppingItem} from '../model/shopping-item';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
cart: ShoppingItem[];
keys: String[];
totPrice = 0;
  constructor(private shopServ: ShoppingCartService) {

  }

  ngOnInit(): void {
    this.shopServ.getCart().valueChanges().subscribe((data: any) => {
      if (data) {
      this.cart = data.items;
      this.keys = Object.keys(this.cart);
      }


      let som = 0;
      if (this.cart) {
for (let e in this.cart){
  som += this.cart[e].product.price * this.cart[e].quantity;
}
      }
      this.totPrice = som;
    });
  }

  removeFromCart(id: string){
    this.shopServ.updateItem(id, this.cart[id].quantity, '-');
  }
  addToCart(id: string){
    this.shopServ.updateItem(id, this.cart[id].quantity, '+');
  }
  validator(){
    for (let e in this.cart){
      if(this.cart[e].quantity)
        return false;
    }
    return true;
  }
  clear(){

    this.cart = this.shopServ.removeAllItems();
    this.keys = null;
  }
}
