import {ShoppingItem} from './shopping-item';

export interface ShoppingCart {
  items: ShoppingItem[
];
}
