import {Product} from './product';

export class ShoppingItem {
  product: Product;
  quantity: number;
  get totPrice(){
return this.quantity * this.product.price;
}
}
