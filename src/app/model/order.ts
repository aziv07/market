import {Shipping} from './shipping';
import {ShoppingItem} from './shopping-item';

export class Order {
  orderDate: Date;
  shipping: Shipping;
  items: ShoppingItem[];
  quantity: number;
  totalPrice: number;
}
