export interface User {
  name: string;
  admin: boolean;
  email: string;
}
