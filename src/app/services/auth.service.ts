import { Injectable } from '@angular/core';

import {AngularFireAuth} from '@angular/fire/auth';
import { Observable} from 'rxjs';
import firebase from 'firebase/app';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, take} from 'rxjs/operators';
import {UserService} from './user.service';
import {User} from '../model/user';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private userServ: UserService, private auth: AngularFireAuth, private route: ActivatedRoute,private router:Router) {
    this.user = auth.authState;
  }


  logout() {

    this.auth.signOut();
this.router.navigate(['login'])

  }


  login() {
    const path = this.route.snapshot.queryParamMap.get('URL') || '/';
    localStorage.setItem('visited', path);
    this.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());

  }


  get AppUser(): Observable<User> {
    return this.user. pipe(switchMap(user => {
      if (user) {
        return this.userServ.getUser(user.uid).valueChanges(); }
      return new Observable<User>(null);

    }));

  }
}
