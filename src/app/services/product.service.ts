import { Injectable } from '@angular/core';

import {AngularFireDatabase} from '@angular/fire/database';
import {Router} from '@angular/router';
import {Product} from '../model/product';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase, private router: Router) { }

  redirection(){
    this.router.navigate(['admin/products']);
  }

  delete(i: number){
    return this.db.object('/products/' + i).remove();
  }

  add(prod){
    this.redirection();
    return this.db.list('/products').push(prod);
  }

  getProducts(){
    return this.db.list('/products');
  }


  getProd(id: string){
    return this.db.object('/products/' + id);
  }


  updateProd(prod, id){
    this.router.navigate(['admin/products']);
    return this.db.list('/products').update(id, prod);
  }
}
