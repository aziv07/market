import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireObject} from '@angular/fire/database';
import firebase from 'firebase/app';

import {User} from '../model/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private db: AngularFireDatabase) { }

  addUser(user: firebase.User){
    this.db.object('users/' + user.uid).update({name: user.displayName, email: user.email});
  }


  getUser(id: string): AngularFireObject<User>{
    return this.db.object('users/' + id);
  }
}
