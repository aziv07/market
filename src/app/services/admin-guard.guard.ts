import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from './auth.service';
import {UserService} from './user.service';
import {exhaustMap, map, switchMap, take} from 'rxjs/operators';
import {User} from '../model/user';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardGuard implements CanActivate {
  constructor(private auth:AuthService,private userServ:UserService) {
  }
  canActivate(): Observable<boolean> {
    return this.auth.AppUser

      .pipe(map(appUser => appUser.admin));
  }

}
