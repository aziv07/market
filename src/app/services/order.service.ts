import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {ShoppingCartService} from './shopping-cart.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private db: AngularFireDatabase, private shopServ: ShoppingCartService) { }

  saveOrder(order){
    this.shopServ.resetCart();
    return this.db.list('/orders').push(order);
  }
  getOrders(){
    return this.db.list('/orders');
  }
  getOrdersByUser(id){
    return this.db.list('/orders', (ref)=>{return ref.equalTo(id).orderByChild('userid')});
  }
}
