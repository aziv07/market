import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Product} from '../model/product';
import {ProductService} from './product.service';
import {take} from 'rxjs/operators';
import {pipe, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private db: AngularFireDatabase, private serv: ProductService) { }


private create(){
  return   this.db.list('/shopping-carts').push({
      dateCreated: new Date().getTime()
    });
}
  getCart(){
   const id = this.getOrCreatId();
   return  this.db.object('/shopping-carts/' + id);
}
  addToCart(p: Product, key: string){
    const cart = this.getOrCreatId();
    return  this.db.object('/shopping-carts/' + cart + '/items/' + key).valueChanges();


  }

resetCart(){
  const cart = this.getOrCreatId();
  return  this.db.object('/shopping-carts/' + cart  ).set({});
}

  setItem(p: Product, id: string){
    const cart = this.getOrCreatId();
    this.db.object('/shopping-carts/' + cart + '/items/' + id).set({product: p, quantity: 1});
  }
  updateItem(id: string, quant: number, op: string){
    const cart = this.getOrCreatId();
    const operat = op === '+' ? 1 : -1;
    this.db.object('/shopping-carts/' + cart + '/items/' + id).update({quantity: quant + operat});
  }
  removeAllItems(){const cart = this.getOrCreatId();
                   this.db.object('/shopping-carts/' + cart ).remove();
                   return null;
  }

public getOrCreatId(){
  const cartId = localStorage.getItem('cartId');
  if (!cartId){
    this.create().then(res => {
      localStorage.setItem('cartId', res.key);
      return res.key;
    });
  }else {
return cartId;
  }
}

  public async getOrCreat(){
    const cartId = localStorage.getItem('cartId');
    if (!cartId){
    const res = await  this.create();
    localStorage.setItem('cartId', res.key);
    return res.key;
    }else {
      return cartId;
    }
  }
}
