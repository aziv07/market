import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {User} from '../model/user';
import {ShoppingCartService} from '../services/shopping-cart.service';
import {ShoppingCart} from '../model/shopping-cart';
import {faLeaf,faShoppingCart} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{
appUser: User;
icon=faLeaf;
icon1=faShoppingCart;
public totQuantity = 0;
  navbarCollapsed = false;
  constructor(private auth: AuthService, private shoppServ: ShoppingCartService) {

    auth.AppUser.subscribe(data => {this.appUser = data; });
  }

  logout(){
this.auth.logout();
this.appUser = null;
  }
/*getQuantity(){
    return this.totQuantity;
}*/
ngOnInit() {
  this.shoppServ.getCart().valueChanges().subscribe((data: ShoppingCart) => {
    this.totQuantity = 0;
    if (data) {
      for (const e in data.items) {
        this.totQuantity += data.items[e].quantity;
      }
    }
  });
}

  switch(){
    this.navbarCollapsed = !this.navbarCollapsed;

  }
}
