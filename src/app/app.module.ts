import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {environment} from '../environments/environment';
import { AppComponent } from './app.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ProductsComponent } from './products/products.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { AdminComponent } from './admin/admin.component';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import {RouterModule} from '@angular/router';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuth, AngularFireAuthModule} from '@angular/fire/auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from './services/auth.service';
import {GuardianGuard} from './services/guardian.guard';
import {UserService} from './services/user.service';
import {AdminGuardGuard} from './services/admin-guard.guard';
import { ProductFormComponent } from './admin/product-form/product-form.component';
import {CategoryService} from './services/category.service';
import {FormsModule} from '@angular/forms';
import {ProductService} from './services/product.service';
import {CustomFormsModule} from 'ng2-validation';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { FilterComponent } from './products/filter/filter.component';
import { ProductComponent } from './products/product/product.component';
import {ShoppingCartService} from './services/shopping-cart.service';
import {OrderService} from "./services/order.service";
import { ShoppingCartSummaryComponent } from './check-out/shopping-cart-summary/shopping-cart-summary.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    AppComponent,
    ShoppingCartComponent,
    ProductsComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    LoginComponent,
    HomeComponent,
    CheckOutComponent,
    NavbarComponent,
    DropDownComponent,
    AdminComponent,
    AdminOrdersComponent,
    AdminProductsComponent,
    ProductFormComponent,
    FilterComponent,
    ProductComponent,
    ShoppingCartSummaryComponent
  ],
  imports: [
    BrowserModule, FormsModule,FontAwesomeModule,
    CustomFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule, NgbModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatRippleModule,
    MatInputModule,
    RouterModule.forRoot([
      { path: '', component: ProductsComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'shopping-cart', component: ShoppingCartComponent },
      { path: 'login', component: LoginComponent },


      { path: 'check-out', component: CheckOutComponent, canActivate: [GuardianGuard] },
      { path: 'order-success/:id', component: OrderSuccessComponent, canActivate: [GuardianGuard] },
      { path: 'my/orders', component: MyOrdersComponent , canActivate: [GuardianGuard]},
      {path: 'admin/products/new', component: ProductFormComponent , canActivate: [GuardianGuard, AdminGuardGuard]},
      {path: 'admin/products/:id', component: ProductFormComponent , canActivate: [GuardianGuard, AdminGuardGuard]},

      { path: 'admin/products', component: AdminProductsComponent , canActivate: [GuardianGuard, AdminGuardGuard]},
      { path: 'admin/orders', component: AdminOrdersComponent, canActivate: [GuardianGuard, AdminGuardGuard]}
    ]),
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [OrderService,ShoppingCartService,AuthService, GuardianGuard , UserService, AdminGuardGuard , CategoryService , ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
