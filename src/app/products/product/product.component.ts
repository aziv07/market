import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {pipe, Subscription} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
@Input()product;
  @Input()key;
  sub: Subscription;
  @Input()shoppingCart;
  constructor(private cartService: ShoppingCartService) { }

  ngOnInit(): void {
  }

  calculQuantity(){

      if (!this.shoppingCart || !this.shoppingCart.items) { return 0; }
      else {
        const item = this.shoppingCart.items[this.key];
        return item ? item.quantity : 0;
      }

  }
addToCart(){
 this.sub = this.cartService.addToCart(this.product, this.key)
   .pipe(take(1)).subscribe((ele: any) => {
     if (!ele){
       this.cartService.setItem(this.product, this.key);
     }else{
       this.cartService.updateItem(this.key, ele.quantity,'+');
     }

   });

  }

  removeFromCart(){
    this.sub = this.cartService.addToCart(this.product, this.key)
      .pipe(take(1)).subscribe((ele: any) => {
        if (!ele){
          this.cartService.setItem(this.product, this.key);
        }else{
          this.cartService.updateItem(this.key, ele.quantity,'-');
        }

      });
  }

}
