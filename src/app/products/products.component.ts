import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from '../services/product.service';
import {Observable, Subscription} from 'rxjs';
import {Product} from '../model/product';
import {CategoryService} from '../services/category.service';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, take} from 'rxjs/operators';
import {ShoppingCartService} from '../services/shopping-cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  implements OnInit, OnDestroy{
products: Product[] = [];
categories = [];
filtredProd = [];
selectedCat = '';
card: any;
keys = [];
sub: Subscription;
  constructor(private shoppingServ: ShoppingCartService, private route: ActivatedRoute , private prodServ: ProductService, private categoryServ: CategoryService) {

    this.prodServ.getProducts().valueChanges().pipe(switchMap((data: Product[]) => {this.products = data;

                                                                                    return route.queryParamMap; })).subscribe(params => {
    this.selectedCat = params.get('category');
    if (this.selectedCat){
      this.filtredProd = this.products.filter(e => e.category.toLowerCase() === this.selectedCat.toLowerCase());
    }else {
      this.filtredProd = this.products;
    }
  });

    prodServ.getProducts().snapshotChanges().subscribe(data => {
      data.forEach(e => this.keys.push(e.key));
    });
    this.categoryServ.getCategories().subscribe(data => {this.categories = data; });




  }

ngOnInit() {
  this.sub = this.shoppingServ.getCart().valueChanges().subscribe((data: any) => {this.card = data;
});


}
ngOnDestroy() {
  this.sub.unsubscribe();
}
}
