
import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderService} from '../../services/order.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-admin-orders',
  templateUrl: './admin-orders.component.html',
  styleUrls: ['./admin-orders.component.css']
})
export class AdminOrdersComponent implements OnInit, OnDestroy{
  orders;
sub: Subscription;
  constructor(private orderService: OrderService) {

  }
  ngOnInit() {
    this.sub = this.orderService.getOrders().valueChanges().subscribe((e:any)=>{this.orders=e;console.log(e);});
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
