import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {Subscription} from 'rxjs';
import {Product} from '../../model/product';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit , OnDestroy{
sub: Subscription;
  sub1: Subscription;
products: Product[] = [];
  dataSource=null;
keys = [];
filter = '';
  displayedColumns=['pic','title','price','category','action'];
  @ViewChild(MatPaginator)paginator: MatPaginator;
  @ViewChild(MatSort)sort: MatSort;
  constructor(private  prodServ: ProductService) {
  this.sub = prodServ.getProducts().snapshotChanges().subscribe(data => { data.forEach(e => this.keys.push(e.key)); });
  this.sub1 = prodServ.getProducts().valueChanges().
  subscribe((data:Product[]) => {
    this.products = data;

    this.dataSource=new MatTableDataSource(this.products);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;});
  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  ngOnInit(): void {
  }

  delete(index: number){
    if (confirm('Do you really want to delete that product ?')) {
    this.prodServ.delete(this.keys[index]);
    }
  }

ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
}
}
