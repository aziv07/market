import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoryService} from '../../services/category.service';
import { Subscription} from 'rxjs';
import {ProductService} from '../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {take} from 'rxjs/operators';
import {Product} from '../../model/product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit , OnDestroy{
categories: Subscription;
cat = [];
keys = [];
id: string;
addMode = true;
product: Product={title: '', imgurl: '',  category: '', price:null};
  constructor(private catServ: CategoryService, private prodServ: ProductService, private route: ActivatedRoute) {
    this.categories = catServ.getCategories().subscribe(actions => {
     this.cat = actions;

     this.cat.forEach(e => {
         switch (e.name){
           case 'Bread': this.keys.push('bread'); break;
           case 'Dairy': this.keys.push('dairy'); break;
           case 'Fruits': this.keys.push('fruits'); break;
           case 'Seasonings & Spices': this.keys.push('seasonings'); break;
           case 'Vegetables': this.keys.push('vegetables'); break;
         }
        });

      });
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id){
      this.addMode = false;
      this.prodServ.getProd(this.id).valueChanges().pipe(take(1)).subscribe((p: {category: string, title: string, imgurl: string, price: number}) => this.product = p );
   }
  }

  ngOnInit(): void {
  }

  add(prod){
    if (this.addMode){this.prodServ.add(this.product); }
    else {
this.prodServ.updateProd(this.product, this.id);
    }


  }


ngOnDestroy() {
    this.categories.unsubscribe();
}
}
