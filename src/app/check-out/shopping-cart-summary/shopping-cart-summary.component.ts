import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-shopping-cart-summary',
  templateUrl: './shopping-cart-summary.component.html',
  styleUrls: ['./shopping-cart-summary.component.css']
})
export class ShoppingCartSummaryComponent implements OnInit {
  @Input()items;
  constructor() { }

  ngOnInit(): void {

  }
  totItems(){
    let tot=0;
    for(let e of this.items){
      tot+=e.quantity;}

    return tot;
  }
totalPrice(){
    let som=0;
    for(let e of  this.items){
      som+=e.product.price*e.quantity;
    }
    return som;
}
}
