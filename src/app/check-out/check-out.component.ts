import { Component, OnInit } from '@angular/core';
import {Shipping} from '../model/shipping';
import {ShoppingCartService} from '../services/shopping-cart.service';
import {ShoppingCart} from '../model/shopping-cart';
import {take} from 'rxjs/operators';
import {Product} from '../model/product';
import {OrderService} from '../services/order.service';
import {ShoppingItem} from '../model/shopping-item';
import {Order} from '../model/order';
import {Item} from '../model/item';
import {AuthService} from '../services/auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit{
  shipping = new Shipping();
   cart= [];
  private user;
constructor(private shopServ: ShoppingCartService, private orderServ: OrderService, private authServ: AuthService,private router:Router) {

}
ngOnInit() {
  this.authServ.user.pipe(take(1)).subscribe(user => {
    this.user=user.uid;
    this.shopServ.getCart().valueChanges().pipe(take(1)).subscribe((res: any) => {
      for (const e in res.items){
        this.cart.push({
           product: {
            title: res.items[e].product.title,
            price: res.items[e].product.price,
            category: res.items[e].product.category,
            imgurl: res.items[e].product.imgurl

          },
          quantity: res.items[e].quantity,
          totalPrice: res.items[e].quantity * res.items[e].product.price}
        );
      }
    });
  });

}

placeOrder() {
const order = {
  userid:this.user,
  orderDate: new Date().getTime(),
    shipping: this.shipping,
      items: this.cart
  };

const key = this.orderServ.saveOrder(order).key;
this.router.navigate(['/order-success',key])
  }
}
