
import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {OrderService} from "../services/order.service";

import firebase from "firebase";
import User = firebase.User;
import {Subscription} from "rxjs";

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit,OnDestroy{
  orders;
sub:Subscription;
  constructor(
    private authService: AuthService,
    private orderService: OrderService) {


  }
  ngOnInit() {
    this.authService.user.subscribe(((u:User) => {this.sub=this.orderService.getOrdersByUser(u.uid)
      .valueChanges()
      .subscribe((res:any)=>{this.orders=res;console.log(this.orders);});}));
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
