import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {UserService} from './services/user.service';
import {ShoppingCartService} from './services/shopping-cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'market';
  sub: Subscription;
  val = null;
  constructor(private auth: AuthService, private shopServ: ShoppingCartService, private router: Router, private userServ: UserService) {
    this.sub = auth.user.subscribe(user => {
      if (user){

      this.router.navigateByUrl(localStorage.getItem('visited'));
      localStorage.removeItem('visited');
      userServ.addUser(user);
      }
    });

  }
 async ngOnInit() {
   this.val = await this.shopServ.getOrCreat();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
}
}
