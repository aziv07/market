// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase :{
    apiKey: "AIzaSyDsDj7v2ArafpA6TgMoFTCat1DT3CnZIX4",
    authDomain: "oshop-67edd.firebaseapp.com",
    projectId: "oshop-67edd",
    storageBucket: "oshop-67edd.appspot.com",
    messagingSenderId: "304049792524",
    appId: "1:304049792524:web:1a2be8e83113e0a4d3b029",
    measurementId: "G-134G5MZLJW"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
